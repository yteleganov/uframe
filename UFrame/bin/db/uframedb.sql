DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS frames;
DROP TABLE IF EXISTS matches;

CREATE TABLE users (
	email CHAR(25),
	password CHAR(16),
	username CHAR(20),
	rank INT,
	u_id MEDIUMINT AUTO_INCREMENT primary key NOT NULL,
	u_time TIMESTAMP
);

CREATE TABLE frames (
	f_img_path VARCHAR(60),
        f_thumbnail_path VARCHAR(60),
        f_hint VARCHAR(60),
	f_id MEDIUMINT AUTO_INCREMENT primary key NOT NULL,
	u_id_fk MEDIUMINT,
	f_time TIMESTAMP
);

CREATE TABLE matches (
	m_img_path VARCHAR(60),
        m_thumbnail_path VARCHAR(60),
	up_score MEDIUMINT DEFAULT 0,
	down_score MEDIUMINT DEFAULT 0,
	m_id MEDIUMINT AUTO_INCREMENT primary key NOT NULL,
	f_id_fk MEDIUMINT,
	u_id_fk MEDIUMINT,
	m_time TIMESTAMP
);
