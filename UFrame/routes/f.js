var express = require('express');
var busboy = require('connect-busboy');
var fs = require('fs');
var moment = require('moment');
var router = express.Router();

var uframedb_lib = require('../bin/db/uframedb_lib');

router.get('/', function(req, res){
  if(req.user.user === undefined){
    res.redirect('/')
  } else {
    res.render('frame', {
      image: '/images/test-frame1.png',
      matches: [],
      title: 'Frame',
      javascripts: [],
      stylesheets: [],
      req: req
    });
  }
});

router.get('/:frameid', function(req, res){
//  console.log("DOT NOTATION: " + req.params.frameid);
//  console.log("FUNCTION NOTATION" + req.param('frameid'));
  if(req.user === undefined){
    res.redirect('/')
  }else{
    uframedb_lib.getFramebyId(req.param('frameid'), function (err, obj, matches) {
      /* OBJECT CONTENTS
       -obj.f_img_path
       -obj.f_thumbbail_path
       -obj.f_hint
       -obj.f_id
       -obj.u_id_fk
       -obj.f_time
       */
     /*
       MATCHES CONTENTS - note* matches is an array of matches unlike obj which is a singular instance
     */
      var pageTitle = "";
      var image = "";
      var matchbox = [];
      if (err) {
        pageTitle = "Frame not found, it's up to you to find it!";
        image = "path/to/unfound frame/image";
      }
      else {
        var q = "SELECT username FROM users WHERE u_id='"+obj.u_id_fk+"'";
        uframedb_lib.query(q,function(err,rows){
          if(err){
            console.log(err);
          }
          else{
            uframedb_lib.getMatchesByFId(req.param('frameid'), function(err, mrows){
              if(err){ console.log(err); }
              else {
                pageTitle = "User " + rows[0].username + "'s Frame";
                image = obj.f_img_path;
                matchbox = mrows;
                var hint=obj.f_hint;
                if(hint===null){
                  hint='No Hint!'
                }
                console.log(matchbox);
                var time = moment(obj.f_time);
                res.render('frame', {
                  title: pageTitle,
                  frame: {
                    userlink: '/u/' + obj.u_id_fk,
                    userimg: '/images/test-profile1.png',
                    username: rows[0].username, //db query for actual name, do the same for pageTitle
                    time: time.fromNow(),
                    image: image,
                    hint: hint
                  },
                  userid: obj.u_id_fk,
                  matches: matchbox,
                  javascripts: ['voting.js', 'match.js'],
                  stylesheets: ['feed.css', 'frame.css'],
                  req: req
                });
              }
            })
          }//else close
        });
      }
    });
  }
});

router.get('/:frameid/match', function(req, res){
  if(req.user === undefined){
    res.redirect('/');
  } else {
    res.render('frupload', {
      title: "You found it!",
      message: "Match attempt",
      javascripts: [],
      stylesheets: [],
      req: req
    });
  }
});


router.post('/frupload-handler', function(req, res) {
  console.log(req.url);
  console.log("THIS IS THE FIRST THING");
  //upload file
  req.pipe(req.busboy);
  req.busboy.on('file', function(fieldname, file, filename) {
    console.log("Uploading: " + filename);
    var imgPath = '/matches/tmp.png';
    var img = file.pipe(
      fs.createWriteStream(__dirname+'/../public'+imgPath)
    );

    //delete file if not an image
    if(!filename.match(/\.(jpg|jpeg|png)$/)) {
      fs.unlink(__dirname+'/../public'+imgPath);
      res.render('upload', {message: 'Upload failed, not an image',
                            title: 'upload',
                            stylesheets: [],
                            javascripts: [],
                            req: req
      });
    }

    console.log(req.param('frameid'));
    var f_id = req.param('frameid');
    var u_id = req.param('userid');

    uframedb_lib.saveMatchtoDb(imgPath, u_id, f_id, function(imgId) {
      var newImgPath = '/matches/m'+imgId+'.png';
      fs.rename(__dirname+'/../public'+imgPath,
                __dirname+'/../public'+newImgPath);
      res.redirect('/feed');
    });
  });
});


module.exports = router;
