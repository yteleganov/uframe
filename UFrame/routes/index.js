var express = require('express');
var router = express.Router();
var fs = require('fs');
var flash = require('connect-flash');
var busboy = require('connect-busboy');
var uframedb_lib = require('../bin/db/uframedb_lib');
var easyimage = require('easyimage');
var dbcredentials = require('../dbcredentials.json');
var mysql = require('mysql'),
    dbOptions = {
      host : 'localhost',
      user : dbcredentials["username"],
      password : dbcredentials["password"],
      port : '3306',
      database : 'uframe'
    };
var myConnection = require('express-myconnection');
var moment = require('moment');
var connection = mysql.createConnection(dbOptions);
connection.connect();

router.use(myConnection(mysql, dbOptions, 'single'));



/* GET home page. */
router.get('/', function(req, res) {

  res.render('index', {
    title: 'UFrame',
    stylesheets: ['index.css'],
    javascripts: ['login.js'],
    req: req
  });
});



router.get('/upload', auth, function(req, res) {
  if(req.user === undefined){
    res.redirect('/');
  } else {
    res.render('upload', {
      title: 'New Frame',
      message: '',
      stylesheets: ['upload.css'],
      javascripts: [],
      req: req
    });
  }
});

router.get('/feed', function(req, res) {
  var frames = [];
  var i=0;
  uframedb_lib.getFrames(function(rows,usernames){ 
    if(rows.length>1){
      rows.forEach(function(item){
        var time = moment(item.f_time);
          var frame = { userlink: '/u/'+item.u_id_fk,
                        userimg:  '/images/test-profile1.png',
                        username: usernames[i++],
                        time: time.fromNow(),
                        framelink: '/f/'+item.f_id,
                        imagepath: item.f_img_path
                      };
        //console.log("Push"+i);
        frames.push(frame);
      });
    }
    res.render('feed', {
        title: 'Browse Frames',
        stylesheets: ['feed.css'],
        javascripts: ['login.js'],
        req: req,
        frames: frames   
    });
  }); 
});

router.get('/profile', auth, function(req, res) {
  if(req.user === undefined){
    res.redirect('/')
  } else {
    res.render('profile', {
      title: 'My Profile',
      stylesheets: ['profile.css'],
      javascripts: ['login.js'],
      req: req
    });
  }
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.get('/signup', function(req, res) {
  res.render('signup', {
    title: 'Sign Up',
    message: '',
    stylesheets: ['signup.css'],
    javascripts: ['login.js'],
    req: req
  });
});

router.post('/signup', function(req, res){
  req.getConnection(function(err,connection) {
    if(err){
      console.log(err);
      res.render('signup', {
        title: 'Sign Up',
        message: 'Error',
        stylesheets: ['signup.css'],
        javascripts: [],
        req: req
      });
    }else{
      var q = "SELECT * FROM users WHERE username='"+req.body.username+"' or email='"+req.body.email+"'";
      connection.query(q, function(err,rows) {
        if(err){
          console.log(err);
        }

        if(rows.length>0){
          //Give a message saying username/email already taken
          res.render('signup', {
            title: 'Sign Up',
            message: 'Username\\Email already used.',
            stylesheets: ['signup.css'],
            javascripts: ['login.js'],
            req: req
          });
        }
      });

      var q = "INSERT INTO users (username,password,email,rank) VALUES ('"+req.body.username+"','"+req.body.password+"','"+req.body.email+"','"+50+"')";
      //console.log("q: "+q+"\n");
      connection.query(q, function(err,rows){
            if(err){
              console.log(err);
            }
            res.render('signup', {
              title: 'Sign Up',
              message: 'You have successfully created an account on UFrame. Time to Log In and Frame!',
              stylesheets: ['signup.css'],
              javascripts: ['login.js'],
              req: req
            });
      });
    }

  });


});


// TODO: please add the call of this query when the frame is successfully uploaded:
// TODO: UPDATE users SET rank=rank+5 WHERE username = req.user.user
router.post('/upload-handler', auth, function(req, res) {
  //upload file
  req.pipe(req.busboy);
  req.busboy.on('file', function(fieldname, file, filename) {
    console.log("Uploading: " + filename);
    var imgPath = '/frames/tmp.png';
    var img = file.pipe(
                   fs.createWriteStream(__dirname+'/../public'+imgPath)
                 );

    //delete file if not an image
    if(!filename.match(/\.(jpg|jpeg|png)$/)) {
      fs.unlink(__dirname+'/../public'+imgPath);
      res.render('upload', {message: 'Upload failed, not an image',
                            title: 'upload',
                            stylesheets: [],
                            javascripts: [],
                            req: req
      });
    }

    //file is an image, so create thumbnail and store paths in db
    //var thumbnailPath = '/thumbnails/tmp_thumbnail.png';
    //var t_options = {
    //  src : __dirname+'/../public'+ imgPath ,
    //  dst : __dirname+'/../public'+ thumbnailPath,
    //  width : 256   , height: 256,
    //  x : 0         , y : 0
    //}
    //easyimage.rescrop(t_options);
    var i_options = {
      src : __dirname+'/../public'+ imgPath ,
      dst : __dirname+'/../public'+ imgPath,
      width : 900   , height: 900,
      x : 0         , y : 0
    }
    easyimage.rescrop(i_options);

    //store image, thumbnail and hint in db
    var hint = req.body.hint;
    uframedb_lib.saveFrametoDb(imgPath, hint, req.user.user, function(imgId) {
      var newImgPath = '/frames/f'+imgId+'.png';
      fs.rename(__dirname+'/../public'+imgPath,
                __dirname+'/../public'+newImgPath);
      res.redirect('../feed');
    });
  });
});
//UNTESTED
router.post('/frupload-handler', function(req, res) {
  //upload file
  req.pipe(req.busboy);
  req.busboy.on('file', function(fieldname, file, filename) {
    console.log("Uploading: " + filename);
    var imgPath = '/frames/tmp.png';
    var img = file.pipe(
                   fs.createWriteStream(__dirname+'/../public'+imgPath)
                 );

    //delete file if not an image
    if(!filename.match(/\.(jpg|jpeg|png)$/)) {
      fs.unlink(__dirname+'/../public'+imgPath);
      res.render('upload', {message: 'Upload failed, not an image',
                            title: 'upload',
                            stylesheets: [],
                            javascripts: [],
                            req: req
      });
    }

    //file is an image, so create thumbnail and store paths in db
    var thumbnailPath = '/thumbnails/tmp_thumbnail.png';
    var options = {
      src : __dirname+'/../public'+ imgPath ,
      dst : __dirname+'/../public'+ thumbnailPath,
      width : 256   , height: 256,
      x : 0         , y : 0
    }
    easyimage.rescrop(options);
    var hint = req.body.hint;
    uframedb_lib.saveFrametoDb(imgPath, thumbnailPath, hint, function(imgId) {
      var newImgPath = '/frames/f'+imgId+'.png';
      var newThumbnailPath = '/thumbnails/t'+imgId+'.png';
      fs.rename(__dirname+'/../public'+imgPath,
                __dirname+'/../public'+newImgPath);
      fs.rename(__dirname+'/../public'+thumbnailPath,
                __dirname+'/../public'+newThumbnailPath);
      res.render('feed', {
        title: 'Browse Frames',
        stylesheets: ['feed.css'],
        javascripts: ['login.js'],
        req: req,
        frames: [{
          userlink: '/u/Johnny_Appleseed',userimg: '/images/test-profile1.png',
          username: 'Johnny Appleseed'   ,time: '2h',
          framelink: '/f/101'       , imagepath: newImgPath
        }]
      });
    });
  });
});


router.get('/leaderboard', auth, function(req, res) {
  if(req.user === undefined){
    res.redirect('/')
  } else {
    req.getConnection(function (err, connection) {
      if (err) {
        console.log(err);
      }
      else {
        var q = "SELECT username, rank FROM users ORDER BY rank DESC LIMIT 15";
        connection.query(q, function (err, rows) {
          if (err) {
            console.log(err);
          }
          else {
            res.render('leaderboard', {
              title: 'Leaderboard',
              message: '',
              stylesheets: ['leaderboard.css'],
              javascripts: ['login.js'],
              req: req,
              data: rows
            });
          }
        });
      }
    });
  }
});

function auth(req, res, next){
  if(req.user==undefined || req.user.user ==undefined){
    res.redirect('/');
  }
  next();
}


module.exports = router;
