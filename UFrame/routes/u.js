var express = require('express');
var router = express.Router();
var uframedb_lib = require('../bin/db/uframedb_lib');
var dbcredentials = require('../dbcredentials.json');
var mysql = require('mysql'),
    dbOptions = {
        host : 'localhost',
        user : dbcredentials["username"],
        password : dbcredentials["password"],
        port : '3306',
        database : 'uframe'
    };
var myConnection = require('express-myconnection');

var connection = mysql.createConnection(dbOptions);
connection.connect();

router.use(myConnection(mysql, dbOptions, 'single'));


router.get('/', auth, function(req, res){
  if(req.user === undefined){
    res.redirect('/')
  } else {
    req.getConnection(function (err, connection) {
      if (err) {
        console.log(err);
      }
      else {
        var q = "SELECT * FROM users WHERE username='" + req.user.user + "'";
        connection.query(q, function (err, rows) {
          if (err) {
            console.log(err);
          }
          else {
            var q2 = "SELECT * FROM frames WHERE u_id_fk='" + rows[0].u_id + "'ORDER BY f_time DESC";
            connection.query(q2, function (err, fr) {
              if (err) {
                console.log(err);
              }
              else {
                res.render('profile', {
                  username: rows[0].username,
                  points: rows[0].rank,
                  propic: "images/test-profile1.png",
                  frames: fr,
                  title: 'Profile',
                  javascripts: [],
                  stylesheets: ['profile.css'],
                  req: req,
                  framelink: '/f/101'
                });
              }
            });
          }
        });
      }
    });
  }
});


/*
router.get('/', auth, function(req, res){

  res.render('profile', {username: 'Johnny Appleseed',
                         propic: "images/test-profile1.png" ,
                         frames: [],
                       title: 'Profile',
                       javascripts: [],
                       stylesheets: ['profile.css'],
                       req: req
                      });
});
*/
router.get('/:userid', auth, function(req, res){


  uframedb_lib.getUserByID(req.param('userid'), function(err, obj){
    //obj contains user info on the followong feilds
    //   --obj.propic : user's profile picture
    //   --obj.frames : user's active submitted frames
    //   --obj.uname  : user's name
    //   --obj.points : user's points (rank attribute)

    var pageTitle= "";
    var image = "";
    var frames = [];
    var points, name;
    if(err) {
      pageTitle = "Unknown user";
      image = "images/test-profile1.png";
    }
    else{
      pageTitle = obj.uname + "'s profile";
      image     = obj.propic;
      frames    = obj.frames;
      points    = obj.points;
      name      = obj.uname;
    }//current issue:  propic path seems to start with /u/ causing a 404
    //but that shouldn't matter once the pictures come from an actual db query
    res.render('profile', { username: name,
                           propic : image,
                           title: pageTitle,
                           frames: frames,
                           points: points,
                           javascripts: ['login.js'],
                           stylesheets:  ['profile.css'],
                           req: req
                       });
  });
});

function auth(req, res, next){
  if(req.user==undefined || req.user.user ==undefined){
    res.redirect('/');
  }
  next();
}

module.exports = router;
