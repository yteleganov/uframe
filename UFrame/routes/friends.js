var express = require('express');
var router = express.Router();

var uframedb_lib = require('../bin/db/uframedb_lib');

router.get('/', auth, function(req, res){
  res.render('friends', {username: 'Johnny Appleseed',
  frames: [],
  title: 'Friends',
  javascripts: [],
  stylesheets: [],
  req: req
});
});

function auth(req, res, next){
  if(req.user==undefined || req.user.user ==undefined){
    res.redirect('/');
  }
  next();
}

module.exports = router;
