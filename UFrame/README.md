# UFrame code

Install the node modules after pulling: `npm install`

--

You MUST indicate your MySQL database credentials in dbcredentials.json. app.js will read this file for your MySQL credentials.
There is a dbcredentials.json.example file, so make a dbcredentials.json file in the UFrame directory and edit it according to your current MySQL setup.  

When working on CSS, edit the .scss files located in `/public/scss`.  
They utilize the SASS precompiler which basically allows you to code CSS in a more developer-friendly way (nesting, importing files, etc)
and then it processes and compiles your .scss files into browser-readable .css in the `/public/stylesheets` folder.  
  
Any edits you make to the regular .css files in `public/stylesheets` will be **overwritten** by the SASS precompiler

--

######Team UFrame
