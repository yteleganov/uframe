The proposal is a creative idea and I believe you all will have fun developing the idea 
throughout the course. 

Everyone's bio's were interesting and complete. It seems like you all will have a 
successful semester working together given skills, past courses, and interests. Also, I like
how your logo is simple clean, and appealing.

Based on your Trello activity, it seems like the communication between your group is 
strong and you are all participating and staying on top of the assignments.

Keep using Trello and good job on project one. You were all very thorough.


------------------------------------------------------------------------------


Very good division of resposibilities and work. Seems like you all have good 
experience for the roles that you will be working on.
Seems like you are off to a very good start and using Trello a good amount. 
Don't archive past Trello posts so I can follow along with your progress and 
keep track who is doing what.

Thumbs up on this submission.
Make sure you are recording key events of all your meetings and keep up with 
weekly meetings.



------------------------------------------------------------------------------


Very good submission! The only thing you are missing is a revision history.
Add the revision history and youll get a thumbs up for the submission.
Keep track of the changes that you make to your idea as you work towards your
final version.

Good meeting notes on github. Continue to meet weekly and keep track of who 
was at the meeting and what was accomplished.
When using Trello, don't archive past boards because this helps track your 
progress. It seems like everyone is putting in an equal amount of work.



------------------------------------------------------------------------------



Thumbs up on this assignment. I am not sure you have a complete understanding of
revision history. The revision history is supposed to keep track of all changes
that you have made since the beginning formation of the idea. 

Good job on continuing to use Trello and keeping track of your meetings. I
am interested to see how the final project turns out!

Your PDF also does a good job highlighting the main points of the write up.
Also, make sure everyone continues to put in an equal amount of work, this will
help you all in the end.


