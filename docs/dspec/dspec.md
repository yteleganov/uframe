
##Overview

UFrame: frame a picture, make a match, get points.  What more do you need to know?  
UFrame is the social app where the users who can match a photo of a location on campus the fastest get points!
You start off in the Frame Feed where all of the matched and unmatched Frames are, if you recognize one of them it's up
to you to reframe the photo and post the match. If it is indeed a match, and you were the first, you collect! Cha-ching!
How many points you get depends on how long the original UFrame remained unmatched and well your photo does compared to
other attempted matches, but if you're the first to a legitimate match, you're guaranteed points.
View your profile to see your activity and how your Frames and matches are doing.  Post your own UFrames with the
'Submit a Frame' button.  Or, shoot over to the leaderboards and see where you rank!
See a submission where the first poster didn't do a great match but someone else did? Let them know by voting their
match as the 'best match' for that submission!
UFrame is a social game that assumes everyone is familiar with and has access to the places that UFrame, so only public
spaces ON THE UMASS AMHERST CAMPUS will be accepted. No dorms, private rooms, or otherwise restricted spaces can be
accepted.  Now get out there and reframe what UFrame!
<sub><sup>(Gabe Markarian, 11/06)</sup></sub>

##Components

#####Homepage
> As soon as users come to uframe.com they are routed to the home page.  This page has two goals, to
  provide an explanation of UFrame like in the overview above, and to lead the user to the sign up or sign in page.
  Ideally, along with a textual description of UFrame, a short video produced by our marketing team will be available to watch as well.

> The 'sign up/sign in' element will have some Javascript/JQuery attached to it so instead of routing to a new page to
  sign in, the form will be available on the same page in the form of a dropdown menu. If the user elects to sign up,
  they simply enter their email, name, and password.  On submit the name is checked against the ones that exist already in
  our SQL user db, and is either created or denied based on if the desired name/email are duplicates.  Logging in is done
  with the Passport npm package.  Our 'strategy' as defined by Passport will be pretty straightforward, simply checking if
  the username and password were entered and authenticating them based on our user db through Passport.  
  <sub><sup>(Gabe Markarian, 11/06)</sup></sub>


#####Users
> Users will be able to sign in/sign up as explained in the Homepage section. Afterward, they will be redirected to their
  Profile page, where they will be able to add/edit a profile picture, add/edit a short description for their profile,
  view their posted Frames and Matches, or start browsing UFrame! Via the persistent navbar on the bottom of the screen, the
  user can browse to anywhere in UFrame. If they want to see everyone's activity, they can go to the Feed. If they want to see their
  standing among their friends or the public, they can navigate to the Leaderboard.If they decide to upload a new Frame, they can hit the Upload
  button from anywhere in UFrame.

> Users will be able to edit their account by clicking on their username in the top right and clicking "Account Settings."
  From there they can change their password, location, or add additional account info.
  <sub><sup>(Yevgeniy Teleganov, 11/09)</sup></sub>


#####Feed
> Users, who come to the Uframe and are already signed up, would definitely like to view and browse the frames and attempts for matching, made by other users.
- *Feed*: this page will show the frames made by all the users. The most "popular" frames will be shown first. The "popularity" of the frames will be based on the ranking of the user, taken the frame, the number of attempts, the number of correct matches and the date the frames was taken.
> <sub><sup>(Alexey Novokshonov, 11/12)</sup></sub>


#####Database
> Our database will be built upon MySQL because of its reliability, and popularity, among web applications. Instructions for how we setup the database are located in UFrame/bin/db/databaseSetup.md. Furthermore, to facilitate connecting to our database, we are using the 'express-connect' package.   
   
> Our implementation uses the following tables:  
- *user*: The user table will store all information about the users such as their username, password, primary key, email address, and a timestamp of when their accound was created.   
- *frame*: The frame table will store the path of the image, a primary key, a foreign key to the user who uploaded it, and a timestamp.   
- *match*: The match table will store the path of the image, a primary key, a foreign key to the frame it is associated with, and a timestamp.  
<sub><sup>(Edward Murphy, 11/12)</sup></sub>   
<sub><sup>(Edward Murphy, 11/25, changed database schema)</sup></sub>   


#####Leaderboard
> The Leaderboard will be a list of users in descending order of their points accumulated, the leaderboard will be limited to one page. The leaderboards will list the top 50 users in our database, if a user would like to see more of the list he can click a button to load more users into the leaderboard. Additionally, a user can search for another user or even themselves in the leaderboard to determine their rank, every UFrame user will have a rank in accordance with the number of points they've accumulated.  
  
> System of point:  
- 5 points - the correct match  
- 10 points - the best match  
- 5 points - the frame that was not matched after the first day of upload  
- 15 poing - the frame that was not matched after the first week of upload  
- 25 points - every 50 uploaded frames  
- 25 points - every 50 uploaded matches  
<sub><sup>(Daniel Lipeles, 11/13)</sup></sub>


#####Matches
> Matches are the core concept of the UFrame app. Each user gets one attempt at a match for each frame. The user can choose to replace an old attempt with a new one, but they can only have one match up for voting per frame. Users compete with each other to match pictures uploaded by other users as closely as possible, and a rewarded points based on rank.
- *Frames*: Frames are the original picture that is uploaded to be matched.
- *Matches*: Matches are the pictures uploaded by other users to be more similar to the Frame then any other user.
- *Match-Rank*: Matches will be ranked by users votes on the matches.
- *Voting*: Users will have the ability to vote on matches based on the similarity to the original. A photo is to be Up voted if it is a better match than the others near it, and Down vote if it's worse.

<sub><sup>(Paul Edwards, 11/13)</sup></sub>

##External Libraries

We will be using the following libraries for development of UFrame:
- *Express*: We will be using Express to generate the framework for our web app, in addition Express simplifies http request and response handling and allows us to concentrate our efforts on higher level development.
- *EJS*: We will use EJS to allow for more dynamic html creation, EJS allows us to pass certain variables into web pages such that we can create custom pages for users, frames, etc.
- *Debug*: We will use Debug to aid us in development, debug allows us to see a stack trace when an error is detected, this greatly improves our capability to fix errors.
- *Node-Sass*: We will use Node-Sass as an extension on CSS, Sass allows us to include variables into CSS which greatly improves our capabilities for design.
- *Passport*: We will use the Passport middleware as a means of handling user authentication, this will provide us with a safe login procedure for our users.   
- *expess-connect*: We will use this middleware to handle making connections to the mysql database.   
- *connect-busboy*: We will use this middleware to handle uploading pictures to the database

<sub><sup>(Daniel Lipeles, 11/13)</sup></sub>
<sub><sup>(Edward Murphy, 11/25/14, added express-connect and connect-busboy)</sup></sub>

##Revision History
|Date     |Version|Description                                                                        |
|:-------:|:-----:|:---------------------------------------------------------------------------------:|
|8/16/14  |0.1    |Proposal                                                                           |
|11/5/14  |0.2    |Functional Specification                                                           |
|11/11/14 |1.0    |Skeleton application running in express with basic routing functionality           |
|11/18/14 |1.1    |Added front-end styling to views, a functioning database, and login authentication |
|11/25/14 |1.2    |Added front-end styling to views, and functionality to upload pictures to db       |
|12/04/14 |2.0    |Removed "Explore" and friends from fspec and dspec, major updates to the app       |

<sub><sup>(Edward Murphy, 11/12)</sup></sub>   
<sub><sup>(Edward Murphy, 11/25/14, added versions 1.0 1.1 1.2)</sup></sub>  
<sub><sup>(Yevgeniy Teleganov, 11/05/14, added version 2.0)</sup></sub>

--

######Team UFrame
