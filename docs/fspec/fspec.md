![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/uframe-logo1.png "Uframe Logo")

###Team Organization
**Yevgeniy Teleganov**  
- Front End Developer/Marketing 
- Responsibilities: Designing and implementing the user experience of the app as well as producing a promotional video   
   
**Alexey Novokshonov**   
- Project Developer   
- Responsibilities: Specifically handling algorithms associated with bigger systems in the UFrame app such as efficient management of global leaderboard data   
  
**Paul Edwards**   
- Usability Manager   
- Responsibilities: Researching and designing how the user interacts with UFrame   
  
**Daniel Lipeles**   
- Backend Developer   
- Responsibilties: Handling interactions between the app and database on the server side, which includes our user authentication system
  
**Edward Murphy**   
- Project Developer    
- Responsibilities: Working on general development of the app wherever needed to ensure the completion of iterations     
  
**Gabriel Markarian**   
- Project Manager   
- Responsibilities: Overseeing the project as well as implementing the system for recognizing two photos as a match
  
<sub><sup>(Edward Murphy, 10/11)</sup></sub>  


--

###Overview

UFrame: frame a picture, make a match, get points.  What more do you need to know?  
UFrame is the social app where the users who can match a photo of a location on campus the fastest get points!  You start off in the Frame Feed where all of the matched and unmatched Frames are, if you recognize one of them it's up to you to reframe the photo and post the match.  If it is indeed a match, and you were the first, you collect! Cha-ching!  How many points you get depends on how long the original UFrame remained unmatched and well your photo does compared to other attempted matches, but if you're the first to a legitimate match, you're guaranteed points.  
View your profile to see your activity and how your Frames and matches are doing.  Post your own Frames with the 'Submit a Frame' button.  Or, shoot over to the leaderboards and see where you rank among your friends or overall!
See a submission where the first poster didn't do a great match but someone else did? Let them know by voting their match as the 'best match' for that submission!  
UFrame is a social game that assumes everyone is familiar with and has access to the places that UFrame, so only public spaces ON THE UMASS AMHERST CAMPUS will be accepted. No dorms, private rooms, or otherwise restricted spaces can be accepted.  Now get out there and reframe what UFrame!  
<sub><sup>(Gabe Markarian, 10/15)</sup></sub>

--

###Scenarios

***Creating a UFrame account***   

Upon navigating to the UFrame web app for the first time, you will arrive at the homepage. Here you can get a glimpse of what UFrame has to offer by viewing our embedded marketing video. But in order to get in on the fun for yourself, you need to first click the “Sign Up” button. This will open a window in which you will be prompted to enter an email address and a password. To submit your registration, you simply need to click the “Create Account” button. Lastly, check your inbox for a confirmation email that will activate your UFrame account and bring you to your new profile page where you can get started!   
<sub><sup>(Edward Murphy, 10/15)</sup></sub>   

***Deleting your UFrame account***   

By the very unlikely chance that you decide that UFrame is not for you, try sleeping it off and seeing how you feel in the morning. However, if you try that and still want to rid your life of UFrame then do not worry! The process of deleting your UFrame account is very simple. All you need to do is follow these steps: login to your account, navigate to your profile page, locate and click on the settings button, and lastly click on “Delete Account”. Upon doing so, you will receive a confirmation email and will be removed from UFrame.   
<sub><sup>(Edward Murphy, 10/15)</sup></sub>  

***Browsing the Frame Feed***

This is the page with the most content and will most likely be the first page you see when you login to, or open the app. This hosts an aggregate of everyone's UFrame activity sorted by weight.  Here the posts are weighted by age and activity.  Recently posted Frames with many attempted matches are at the top and old matches with few attempts are at the bottom.  The user can elect to see only Frames with no matches yet as well by clicking the "Show unmatched" button.
<sub><sup>(Gabe Markarian, 10/15)</sup></sub>

***Viewing the Leaderboard***

The leaderboard page is where everyone can see where they rank among the whole community.  The leaderboard on arrival shows the top 15 people's rank in the community.  Each rank has the username and number of points the user has accumulated.  Finally, the user can elect to see the entire list of people in the selected scope, instead of just 1 through 10 and themselves.  Eventually, a loading animation will need to be implemented so that querying the server for the complete list of ranks does not cause a laggy behavior.
<sub><sup>(Gabe Markarian, 10/15)</sup></sub>

***Voting on Matches***

Upon clicking on a Frame, you'll be presented with a magnified version of the Frame and its Matches next to it. Voting on a Match is really simple. You can vote one "Best Match" per Frame and you can upvote as many Matches as you like. If a Match actually happens to be something totally different, you can vote "Not a Match". Enough votes for "Not a Match" will discard the Match from the original Frame.  
<sub><sup>(Yevgeniy Teleganov, 10/16)</sup></sub>

***Upload an Original Photo***

The key feature of Uframe is the upload/match system, and the uploading part is as easy as a few clicks of the mouse! From any page, click on the Upload Frame button in the navigation bar, and a file browser will open up allowing you to select and upload your photo. Once you've chosen your desired frame, the race is on and other users are challeneged to match the frame!  
<sub><sup>(Daniel Lipeles, 10/16)</sup></sub>

***Upload a match to another original photo "frame"***

Uploading a match is as easy as uploading an original frame! Find a frame you want to match in your feed and click on it, a prompt will open allowing you to select your photo from the file manager and upload it to Uframe. Points will be awarded in accordance with your speed and the race to score is on!  
<sub><sup>(Daniel Lipeles, 10/16)</sup></sub>

--

###Non-Goals

In this version we will not be concerned with:

- Photos from outside of the campus of the University of Massachusetts. Our userbase will be limited to Umass students. 

- We will not be developing a mobile app at this time, submission will be done through the web app via a web browser. 

- Social Media integration; we will have our own database of users for the first iteration.

- Image recognizing algorithms, matches will be verified manually for the first version.

- Implementing friends since it's best to have a totally public feed in the starting stages of release.

<sub><sup>(Daniel Lipeles, 10/16)</sup></sub>  

--

###Flowchart

![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/TA3_flowcharts_700px.png "Flowchart")

<sub><sup>(Alexey Novokshonov, 10/14)</sup></sub>  


--

###Screen-by-Screen

####Homepage
>![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/ta3_wireframe_homepage.png "Homepage Wireframe")  
>This is the UFrame homepage. From here you will be able to view our marketing video, read about all the possibilities of UFrame and finally sign up or sign in!

####Sign up / Sign in
>![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/ta3_wireframe_signup.png "Sign up/Sign in Wireframe")  
>To sign up or sign in to UFrame, simply click either of the buttons in the top right and enter your information in the dropdown menu.

####User Profile
>![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/ta3_wireframe_profile.png "User Profile Wireframe")  
>This is the UFrame profile page, where you can find a specific user's Frames and Matches, along with their Leaderboard statistics. The navigation bar is consistent throughout UFrame for easy access to all the main features.

####Uploading a Frame
>![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/ta3_wireframe_upload.png "Uploading a Frame Wireframe")  
>You can upload a Frame from anywhere in UFrame. Simply hit the "Upload" button in the navigation bar and you will be shown a prompt to upload a photo or even take a photo on the spot!

####Frame Feed
>![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/ta3_wireframe_framefeed.png "Frame Feed Wireframe")  
>The Frame Feed is your central hub for viewing everyone's Frames and Matches.

####Voting on Matches
>![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/ta3_wireframe_vote.png "Voting Wireframe")  
>After clicking on a Frame anywhere in UFrame, a window appears where you can view the Frame and its individual Matches. Here you can vote on Matches by either upvoting a match, selecting one Match as a "Best Match" or designating a Match as "Not a Match."

<sub><sup>(Yevgeniy Teleganov, 10/16)</sup></sub>  

--

###Open Issues:


- Points System:
 - 1 point for the first match(good match or any match?)
 - 1 point for the best match?
 
- Limited Submissions
 - Only 1 active(voting) submission at a time per Frame.
 - Can change attempt after first submission.

- Voting for Matches
 - Up/Down?
 - Not a Match

- Leaderboards
 - Sortable leaderboards
  - Friends
  - Global


- Website Feed based off of Geo-Location?   


- Use of geotag for pic match verification?

<sub><sup>(Paul Edwards, 10/15)</sup></sub>   

--

###Revision History:   
|Date     |Version|Description                                                                        |
|:-------:|:-----:|:---------------------------------------------------------------------------------:|
|8/16/14  |0.1    |Proposal                                                                           |
|11/5/14  |0.2    |Functional Specification                                                           |
|11/11/14 |1.0    |Skeleton application running in express with basic routing functionality           |
|11/18/14 |1.1    |Added front-end styling to views, a functioning database, and login authentication |
|11/25/14 |1.2    |Added front-end styling to views, and functionality to upload pictures to db       |
|12/04/14 |2.0    |Removed "Explore" and friends from fspec and dspec, major updates to the app       |

<sub><sup>(Edward Murphy, 11/5)</sup></sub>   
<sub><sup>(Edward Murphy, 11/25, added versions 1.0 1.1 1.2)</sup></sub>
<sub><sup>(Yevgeniy Teleganov, 11/05/14, added version 2.0)</sup></sub>